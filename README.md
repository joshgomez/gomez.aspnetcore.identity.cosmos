
# Gomez.AspNetCore.Identity.Cosmos
See the commit code details for changelog.

For best scaling partition key is by normalized name which should be the email.
Lookup documents is used to find the document when partition key is unknown.

Currently this provider does not support query all users because it will be a cross partition query which can be super expensive.
Best way is to have a tenant/customer/application collection where all users is replicated to in a single partition.
This will however only support query all users per tenant and not in the whole system which is good practise 
by security point of view.


Todo:
Add method for manually sync lookup documents, this can be handy if documents get edited by external sources and in the future we could delete inactive documents in the lookup partition using ttl.

## Dependency
Gomez.Core.DocumentDb.Cosmos is for implementing the store.
Cosmos .NET SDK3


## Usage

	using Gomez.AspNetCore.Identity.Cosmos.Extensions
	AddDefaultDocumentClientForIdentity()
	AddDocumentDbStores()