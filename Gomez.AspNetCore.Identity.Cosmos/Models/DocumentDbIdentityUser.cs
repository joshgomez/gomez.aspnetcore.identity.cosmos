﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace Gomez.AspNetCore.Identity.Cosmos.Models
{
    /// <summary>
    /// Represents a user in the identity system for the <see cref="Stores.DocumentDbUserStore{TUser, TRole}"/> with the role type defaulted to <see cref="DocumentDbIdentityRole"/>
    /// </summary>
    public class DocumentDbIdentityUser : DocumentDbIdentityUser<DocumentDbIdentityRole>
    {
    }

    /// <summary>
    /// Represents a user in the identity system for the <see cref="Stores.DocumentDbUserStore{TUser, TRole}"/>
    /// </summary>
    public class DocumentDbIdentityUser<TRole> : Core.DocumentDb.DefaultDocumentEntity, Core.DocumentDb.IPartitionedDocumentEntity
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public DocumentDbIdentityUser()
        {
            Roles = new List<TRole>();
            Logins = new List<UserLoginInfo>();
            Claims = new List<Claim>();
            Type = GetType().Name;
        }

        /// <summary>
        /// UserName
        /// </summary>
        [JsonProperty(PropertyName = "userName")]
        public string UserName { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }

        /// <summary>
        /// NormalizedUserName
        /// </summary>
        [JsonProperty(PropertyName = "normalizedUserName")]
        public string NormalizedUserName { get; set; }

        /// <summary>
        /// NormalizedEmail
        /// </summary>
        [JsonProperty(PropertyName = "normalizedEmail")]
        public string NormalizedEmail { get; set; }

        /// <summary>
        /// IsEmailConfirmed
        /// </summary>
        [JsonProperty(PropertyName = "isEmailConfirmed")]
        public bool IsEmailConfirmed { get; set; }

        /// <summary>
        /// PhoneNumber
        /// </summary>
        [JsonProperty(PropertyName = "phoneNumber")]
        public string PhoneNumber { get; internal set; }

        /// <summary>
        /// IsPhoneNumberConfirmed
        /// </summary>
        [JsonProperty(PropertyName = "isPhoneNumberConfirmed")]
        public bool IsPhoneNumberConfirmed { get; internal set; }

        /// <summary>
        /// PasswordHash
        /// </summary>
        [JsonProperty(PropertyName = "passwordHash")]
        public string PasswordHash { get; set; }

        /// <summary>
        /// SecurityStamp
        /// </summary>
        [JsonProperty(PropertyName = "securityStamp")]
        public string SecurityStamp { get; set; }

        /// <summary>
        /// IsTwoFactorAuthEnabled
        /// </summary>
        [JsonProperty(PropertyName = "isTwoFactorAuthEnabled")]
        public bool IsTwoFactorAuthEnabled { get; set; }

        /// <summary>
        /// Logins
        /// </summary>
        [JsonProperty(PropertyName = "logins")]
        public IList<UserLoginInfo> Logins { get; set; }

        /// <summary>
        /// Roles
        /// </summary>
        [JsonProperty(PropertyName = "roles")]
        public IList<TRole> Roles { get; set; }

        /// <summary>
        /// Claims
        /// </summary>
        [JsonProperty(PropertyName = "claims")]
        public IList<Claim> Claims { get; set; }

        /// <summary>
        /// LockoutEnabled
        /// </summary>
        [JsonProperty(PropertyName = "lockoutEnabled")]
        public bool LockoutEnabled { get; set; }

        /// <summary>
        /// LockoutEndDate
        /// </summary>
        [JsonProperty(PropertyName = "lockoutEndDate")]
        public DateTimeOffset? LockoutEndDate { get; set; }

        /// <summary>
        /// AccessFailedCount
        /// </summary>
        [JsonProperty(PropertyName = "accessFailedCount")]
        public int AccessFailedCount { get; set; }

        /// <summary>
        /// AuthenticatorKey
        /// </summary>
        [JsonProperty(PropertyName = "authenticatorKey")]
        public string AuthenticatorKey { get; set; }

        /// <summary>
        /// RecoveryCodes
        /// </summary>
        [JsonProperty(PropertyName = "recoveryCodes")]
        public IEnumerable<string> RecoveryCodes { get; set; }

        private string _partitionKey;

        /// <summary>
        /// PartitionKey
        /// </summary>
        [JsonProperty(PropertyName = "partitionKey")]
        public string PartitionKey
        {
            get
            {
                if (_partitionKey == null)
                {
                    _partitionKey = NormalizedUserName;
                }

                return _partitionKey;
            }
            set
            {
                if (_partitionKey == value) return;
                _partitionKey = value;
            }
        }
    }
}