﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Security.Claims;

namespace Gomez.AspNetCore.Identity.Cosmos.Models
{
    /// <summary>
    /// The standard role class, can be be extened.
    /// </summary>
    public class DocumentDbIdentityRole : Core.DocumentDb.DefaultDocumentEntity, Core.DocumentDb.IPartitionedDocumentEntity
    {
        /// <summary>
        ///
        /// </summary>
        public DocumentDbIdentityRole()
        {
            Claims = new List<Claim>();
            Type = GetType().Name;
        }

        /// <summary>
        /// Name
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// NormalizedName, used for finding the role by name.
        /// </summary>
        [JsonProperty(PropertyName = "normalizedName")]
        public string NormalizedName { get; set; }

        /// <summary>
        /// Claims
        /// </summary>
        [JsonProperty(PropertyName = "claims")]
        public IList<Claim> Claims { get; set; }

        private string _partitionKey;

        /// <summary>
        /// PartitionKey, should not be set outside the store if you don't recreate the document.
        /// </summary>
        [JsonProperty(PropertyName = "partitionKey")]
        public string PartitionKey
        {
            get
            {
                if (_partitionKey == null)
                {
                    _partitionKey = NormalizedName;
                }

                return _partitionKey;
            }
            set
            {
                if (_partitionKey == value) return;
                _partitionKey = value;
            }
        }
    }
}