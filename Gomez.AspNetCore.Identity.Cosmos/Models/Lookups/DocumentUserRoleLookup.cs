﻿using Gomez.Core.DocumentDb;
using Newtonsoft.Json;

namespace Gomez.AspNetCore.Identity.Cosmos.Models.Lookups
{
    internal class DocumentUserRolesLookup : DocumentEntity, IPartitionedDocumentEntity
    {
        public DocumentUserRolesLookup()
        {
        }

        [JsonProperty(PropertyName = "normalizedUserName")]
        public string NormalizedUserName { get; set; }

        [JsonProperty(PropertyName = "normalizedRoleNames")]
        public string[] NormalizedRoleNames { get; set; }

        private string _partitionKey;

        [JsonProperty(PropertyName = "partitionKey")]
        public string PartitionKey
        {
            get
            {
                if (_partitionKey == null)
                {
                    _partitionKey = nameof(DocumentUserRolesLookup);
                }

                return _partitionKey;
            }
            set
            {
                if (_partitionKey == value) return;
                _partitionKey = value;
            }
        }
    }
}