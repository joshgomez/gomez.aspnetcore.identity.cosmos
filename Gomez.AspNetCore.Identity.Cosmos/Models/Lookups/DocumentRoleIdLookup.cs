﻿using Gomez.Core.DocumentDb;
using Newtonsoft.Json;

namespace Gomez.AspNetCore.Identity.Cosmos.Models.Lookups
{
    internal class DocumentRoleIdLookup : DocumentEntity, IPartitionedDocumentEntity
    {
        [JsonProperty(PropertyName = "normalizedName")]
        public string NormalizedName { get; set; }

        private string _partitionKey;

        [JsonProperty(PropertyName = "partitionKey")]
        public string PartitionKey
        {
            get
            {
                if (_partitionKey == null)
                {
                    _partitionKey = nameof(DocumentRoleIdLookup);
                }

                return _partitionKey;
            }
            set
            {
                if (_partitionKey == value) return;
                _partitionKey = value;
            }
        }
    }
}