﻿using Gomez.Core.DocumentDb;
using Newtonsoft.Json;

namespace Gomez.AspNetCore.Identity.Cosmos.Models.Lookups
{
    internal class DocumentUserClaimsLookup : DocumentEntity, IPartitionedDocumentEntity
    {
        internal class ClaimLookup
        {
            public string Type { get; set; }
            public string Value { get; set; }
        }

        public DocumentUserClaimsLookup()
        {
        }

        [JsonProperty(PropertyName = "normalizedUserName")]
        public string NormalizedUserName { get; set; }

        [JsonProperty(PropertyName = "normalizedRoleNames")]
        public ClaimLookup[] NormalizedClaims { get; set; }

        private string _partitionKey;

        [JsonProperty(PropertyName = "partitionKey")]
        public string PartitionKey
        {
            get
            {
                if (_partitionKey == null)
                {
                    _partitionKey = nameof(DocumentUserClaimsLookup);
                }

                return _partitionKey;
            }
            set
            {
                if (_partitionKey == value) return;
                _partitionKey = value;
            }
        }
    }
}