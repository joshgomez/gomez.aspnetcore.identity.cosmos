﻿using Gomez.Core.DocumentDb;
using Newtonsoft.Json;

namespace Gomez.AspNetCore.Identity.Cosmos.Models.Lookups
{
    internal class DocumentUserLoginLookup : DocumentEntity, IPartitionedDocumentEntity
    {
        [JsonProperty(PropertyName = "normalizedUserName")]
        public string NormalizedUserName { get; set; }

        private string _partitionKey;

        [JsonProperty(PropertyName = "partitionKey")]
        public string PartitionKey
        {
            get
            {
                if (_partitionKey == null)
                {
                    _partitionKey = nameof(DocumentUserLoginLookup);
                }

                return _partitionKey;
            }
            set
            {
                if (_partitionKey == value) return;
                _partitionKey = value;
            }
        }
    }
}