﻿using Gomez.Core.DocumentDb;
using System;
using System.Linq.Expressions;

namespace Gomez.AspNetCore.Identity.Cosmos.Stores
{
    /// <summary>
    /// StoreBase is used internaly
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class StoreBase<T> : DocumentRepository<T> where T : IDocumentEntity
    {
        /// <summary>
        /// only for implemeting the interface, not used.
        /// </summary>
        protected bool disposed = false;

        /// <summary>
        /// Constuctor of the base class
        /// </summary>
        /// <param name="context"></param>
        /// <param name="collectionName"></param>
        /// <param name="partitionKeyExp"></param>
        protected StoreBase(IDocumentDbContext context, string collectionName, Expression<Func<T, object>> partitionKeyExp) : base(context, collectionName, partitionKeyExp)
        {
        }

        /// <summary>
        /// Used by the classes which inherit this base class.
        /// </summary>
        protected virtual void ThrowIfDisposed()
        {
            if (disposed || _context == null)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }
    }
}