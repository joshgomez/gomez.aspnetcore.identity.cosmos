﻿using Gomez.AspNetCore.Identity.Cosmos.Models;
using Gomez.AspNetCore.Identity.Cosmos.Models.Lookups;
using Gomez.Core.DocumentDb;
using Microsoft.AspNetCore.Identity;
using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Gomez.AspNetCore.Identity.Cosmos.Stores
{
    /// <summary>
    /// Represents a DocumentDb-based persistence store for ASP.NET Core Identity users with the role type defaulted to <see cref="DocumentDbIdentityRole"/>
    /// </summary>
    /// <typeparam name="TUser">The type representing a user</typeparam>
    public class DocumentDbUserStore<TUser> : DocumentDbUserStore<TUser, DocumentDbIdentityRole>
        where TUser : DocumentDbIdentityUser, IPartitionedDocumentEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentDbUserStore{TUser}"/>
        /// </summary>
        /// <param name="dbContext">The DocumentDb client to be used</param>
        /// <param name="options">The configuraiton options for the <see cref="IDocumentClient"/></param>
        /// <param name="roleStore">The <see cref="IRoleStore{TRole}"/> to be used for storing and retrieving roles for the user</param>
        public DocumentDbUserStore(IDocumentDbContext dbContext, IOptions<DocumentDbOptions> options, IRoleStore<DocumentDbIdentityRole> roleStore)
            : base(dbContext, options, roleStore)
        {
        }
    }

    /// <summary>
    /// Represents a DocumentDb-based persistence store for ASP.NET Core Identity users
    /// </summary>
    /// <typeparam name="TUser">The type representing a user</typeparam>
    /// <typeparam name="TRole">The type representing a role</typeparam>
    public class DocumentDbUserStore<TUser, TRole> :
        StoreBase<TUser>,
        IUserClaimStore<TUser>,
        IUserLoginStore<TUser>,
        IUserRoleStore<TUser>,
        IUserPasswordStore<TUser>,
        IUserSecurityStampStore<TUser>,
        IUserTwoFactorStore<TUser>,
        IUserPhoneNumberStore<TUser>,
        IUserEmailStore<TUser>,
        IUserAuthenticatorKeyStore<TUser>,
        IUserTwoFactorRecoveryCodeStore<TUser>,
        IUserLockoutStore<TUser>
        where TUser : DocumentDbIdentityUser<TRole>, IPartitionedDocumentEntity
        where TRole : DocumentDbIdentityRole
    {
        private readonly IRoleStore<TRole> roleStore;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentDbUserStore{TUser, TRole}"/>
        /// </summary>
        /// <param name="dbContext">The DocumentDb context to be used</param>
        /// <param name="options">The configuraiton options for the <see cref="IDocumentClient"/></param>
        /// <param name="roleStore">The <see cref="IRoleStore{TRole}"/> to be used for storing and retrieving roles for the user</param>
        public DocumentDbUserStore(IDocumentDbContext dbContext, IOptions<DocumentDbOptions> options, IRoleStore<TRole> roleStore)
            : base(dbContext, options.Value.UserStoreDocumentCollection, x => x.PartitionKey)
        {
            this.roleStore = roleStore;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="claims"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task AddClaimsAsync(TUser user, IEnumerable<Claim> claims, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            if (claims == null)
            {
                throw new ArgumentNullException(nameof(claims));
            }

            return AddClaimsInternalAsync(user, claims);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="login"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task AddLoginAsync(TUser user, UserLoginInfo login, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            if (login == null)
            {
                throw new ArgumentNullException(nameof(login));
            }

            return AddLoginInternalAsync(user, login);
        }

        private async Task AddLoginInternalAsync(TUser user, UserLoginInfo login)
        {
            user.Logins.Add(login);
            await UpdatePartitionLoginsLookupAsync(user);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="roleName"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task AddToRoleAsync(TUser user, string roleName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            if (roleName == null)
            {
                throw new ArgumentNullException(nameof(roleName));
            }

            return AddToRoleInternalAsync(user, roleName, cancellationToken);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<int> CountCodesAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            var recoveryCodesCount = user.RecoveryCodes?.Count();
            return Task.FromResult(recoveryCodesCount ?? 0);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public new Task<IdentityResult> CreateAsync(TUser user)
        {
            return CreateInternalAsync(user);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<IdentityResult> CreateAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            // If no UserId was supplied, generate a GUID
            if (string.IsNullOrEmpty(user.Id))
            {
                user.Id = Guid.NewGuid().ToString();
            }

            return CreateInternalAsync(user);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public new Task<IdentityResult> DeleteAsync(TUser user)
        {
            return DeleteInternalAsync(user);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<IdentityResult> DeleteAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return DeleteInternalAsync(user);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="normalizedEmail"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<TUser> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (normalizedEmail == null)
            {
                throw new ArgumentNullException(nameof(normalizedEmail));
            }

            normalizedEmail = normalizedEmail.ToUpperInvariant();
            return FindByEmailInternalAsync(normalizedEmail);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<TUser> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (userId == null)
            {
                throw new ArgumentNullException(nameof(userId));
            }

            return FindByIdInternalAsync(userId);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="loginProvider"></param>
        /// <param name="providerKey"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<TUser> FindByLoginAsync(string loginProvider, string providerKey, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (loginProvider == null)
            {
                throw new ArgumentNullException(nameof(loginProvider));
            }

            if (providerKey == null)
            {
                throw new ArgumentNullException(nameof(providerKey));
            }

            return FindByLoginInternalAsync(loginProvider, providerKey);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="normalizedUserName"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<TUser> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (normalizedUserName == null)
            {
                throw new ArgumentNullException(nameof(normalizedUserName));
            }

            var queryOptions = new QueryOptions() { PartitionKey = normalizedUserName, MaxItemCount = 1 };
            var query = GetQuery(queryOptions)
                .Where(u => u.NormalizedUserName == normalizedUserName && u.Type == typeof(TUser).Name);

            return ReadFromQueryAsync(query, queryOptions, new ItemOptions());
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<int> GetAccessFailedCountAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.AccessFailedCount);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<string> GetAuthenticatorKeyAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.AuthenticatorKey);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<IList<Claim>> GetClaimsAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.Claims);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<string> GetEmailAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.Email);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<bool> GetEmailConfirmedAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.IsEmailConfirmed);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<bool> GetLockoutEnabledAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.LockoutEnabled);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<DateTimeOffset?> GetLockoutEndDateAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.LockoutEndDate);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<IList<UserLoginInfo>> GetLoginsAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.Logins);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<string> GetNormalizedEmailAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.NormalizedEmail);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<string> GetNormalizedUserNameAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.NormalizedUserName);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<string> GetPasswordHashAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.PasswordHash);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<string> GetPhoneNumberAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.PhoneNumber);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<bool> GetPhoneNumberConfirmedAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.IsPhoneNumberConfirmed);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<IList<string>> GetRolesAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            IList<string> userRoles = user.Roles.Select(r => r.Name).ToList();

            return Task.FromResult(userRoles);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<string> GetSecurityStampAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.SecurityStamp);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<bool> GetTwoFactorEnabledAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.IsTwoFactorAuthEnabled);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<string> GetUserIdAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.Id);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<string> GetUserNameAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.UserName);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="claim"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IList<TUser>> GetUsersForClaimAsync(Claim claim, CancellationToken cancellationToken)
        {
            var ret = new List<TUser>();
            await foreach (var user in GetUsersForClaimAsAsyncEnumerable(claim, cancellationToken))
            {
                ret.Add(user);
            }

            return ret;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="roleName"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<IList<TUser>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
        {
            var ret = new List<TUser>();
            await foreach (var user in GetUsersInRoleAsAsyncEnumerable(roleName, cancellationToken))
            {
                ret.Add(user);
            }

            return ret;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<bool> HasPasswordAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return Task.FromResult(user.PasswordHash != null);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<int> IncrementAccessFailedCountAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            user.AccessFailedCount++;

            return Task.FromResult(user.AccessFailedCount);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="roleName"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<bool> IsInRoleAsync(TUser user, string roleName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            if (roleName == null)
            {
                throw new ArgumentNullException(nameof(roleName));
            }

            roleName = roleName.ToUpperInvariant();
            bool isInRole = user.Roles.Any(r => r.NormalizedName.Equals(roleName));

            return Task.FromResult(isInRole);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="code"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<bool> RedeemCodeAsync(TUser user, string code, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return RedeemCodeInternalAsync(user, code, cancellationToken);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="claims"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task RemoveClaimsAsync(TUser user, IEnumerable<Claim> claims, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            if (claims == null)
            {
                throw new ArgumentNullException(nameof(claims));
            }

            return RemoveClaimsInternalAsync(user, claims);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="roleName"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task RemoveFromRoleAsync(TUser user, string roleName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            if (roleName == null)
            {
                throw new ArgumentNullException(nameof(roleName));
            }

            return RemoveFromRoleInternalAsync(user, roleName);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="loginProvider"></param>
        /// <param name="providerKey"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task RemoveLoginAsync(TUser user, string loginProvider, string providerKey, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            if (loginProvider == null)
            {
                throw new ArgumentNullException(nameof(loginProvider));
            }

            if (providerKey == null)
            {
                throw new ArgumentNullException(nameof(providerKey));
            }

            return RemoveLoginInternalAsync(user, loginProvider, providerKey);
        }

        private async Task RemoveLoginInternalAsync(TUser user, string loginProvider, string providerKey)
        {
            UserLoginInfo userLoginToRemove = user.Logins
                .FirstOrDefault(l => l.LoginProvider == loginProvider && l.ProviderKey == providerKey);

            if (userLoginToRemove != null)
            {
                user.Logins.Remove(userLoginToRemove);
                await UpdatePartitionLoginsLookupAsync(user);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="claim"></param>
        /// <param name="newClaim"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task ReplaceClaimAsync(TUser user, Claim claim, Claim newClaim, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            if (claim == null)
            {
                throw new ArgumentNullException(nameof(claim));
            }

            if (newClaim == null)
            {
                throw new ArgumentNullException(nameof(newClaim));
            }

            return ReplaceClaimInternalAsync(user, claim, newClaim);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="recoveryCodes"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task ReplaceCodesAsync(TUser user, IEnumerable<string> recoveryCodes, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            user.RecoveryCodes = recoveryCodes;

            return Task.CompletedTask;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task ResetAccessFailedCountAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            user.AccessFailedCount = 0;

            return Task.CompletedTask;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="key"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task SetAuthenticatorKeyAsync(TUser user, string key, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            user.AuthenticatorKey = key;

            return Task.CompletedTask;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="email"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task SetEmailAsync(TUser user, string email, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            user.Email = email;

            return Task.FromResult(user.Email);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="confirmed"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task SetEmailConfirmedAsync(TUser user, bool confirmed, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            user.IsEmailConfirmed = confirmed;

            return Task.FromResult(user.Email);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="enabled"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task SetLockoutEnabledAsync(TUser user, bool enabled, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            user.LockoutEnabled = enabled;

            return Task.CompletedTask;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="lockoutEnd"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task SetLockoutEndDateAsync(TUser user, DateTimeOffset? lockoutEnd, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            user.LockoutEndDate = lockoutEnd;

            return Task.CompletedTask;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="normalizedEmail"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task SetNormalizedEmailAsync(TUser user, string normalizedEmail, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            user.NormalizedEmail = normalizedEmail;

            return Task.FromResult(user.Email);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="normalizedName"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task SetNormalizedUserNameAsync(TUser user, string normalizedName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            user.NormalizedUserName = normalizedName ?? throw new ArgumentNullException(nameof(normalizedName));

            return Task.CompletedTask;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="passwordHash"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task SetPasswordHashAsync(TUser user, string passwordHash, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            user.PasswordHash = passwordHash ?? throw new ArgumentNullException(nameof(passwordHash));

            return Task.CompletedTask;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task SetPhoneNumberAsync(TUser user, string phoneNumber, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            user.PhoneNumber = phoneNumber;

            return Task.CompletedTask;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="confirmed"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task SetPhoneNumberConfirmedAsync(TUser user, bool confirmed, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            user.IsPhoneNumberConfirmed = confirmed;

            return Task.CompletedTask;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="stamp"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task SetSecurityStampAsync(TUser user, string stamp, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            user.SecurityStamp = stamp;

            return Task.CompletedTask;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="enabled"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task SetTwoFactorEnabledAsync(TUser user, bool enabled, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            user.IsTwoFactorAuthEnabled = enabled;

            return Task.CompletedTask;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="userName"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task SetUserNameAsync(TUser user, string userName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            user.UserName = userName ?? throw new ArgumentNullException(nameof(userName));

            return Task.CompletedTask;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<IdentityResult> UpdateAsync(TUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            return UpdateInternalAsync(user);
        }

        private async Task AddClaimsInternalAsync(TUser user, IEnumerable<Claim> claims)
        {
            foreach (Claim newClaim in claims)
            {
                user.Claims.Add(newClaim);
            }

            await UpdatePartitionClaimsLookupAsync(user);
        }

        private async Task AddToRoleInternalAsync(TUser user, string roleName, CancellationToken cancellationToken)
        {
            // Check if the given role name exists
            roleName = roleName.ToUpperInvariant();
            TRole foundRole = await roleStore.FindByNameAsync(roleName, cancellationToken);

            if (foundRole == null)
            {
                throw new ArgumentException(nameof(roleName), $"The role with the given name {roleName} does not exist");
            }

            user.Roles.Add(foundRole);
            await UpdatePartitionRolesLookupAsync(user);
        }

        private async Task<IdentityResult> CreateInternalAsync(TUser user)
        {
            try
            {
                await UpdatePartitionLookupAsync(user);
                await CreateAsync(CreateOptions(user, new ItemOptions() { EnableContentResponseOnWrite = false }), user);
                return IdentityResult.Success;
            }
            catch (CosmosException ex)
            {
                return IdentityResult.Failed(new IdentityError() { Description = ex.Message.ToString(), Code = ex.StatusCode.ToString() });
            }
        }

        private async Task<IdentityResult> DeleteInternalAsync(TUser user)
        {
            try
            {
                await DeleteAsync(user);
            }
            catch (CosmosException dce)
            {
                if (dce.StatusCode == HttpStatusCode.NotFound)
                {
                    return IdentityResult.Failed(new IdentityError() { Description = dce.Message, Code = dce.StatusCode.ToString() });
                }

                throw;
            }

            return IdentityResult.Success;
        }

        private async Task<TUser> FindByEmailInternalAsync(string normalizedEmail)
        {
            var lookup = await GetNormalizedNameByEmailAsync(normalizedEmail);
            var queryOptions = new QueryOptions() { MaxItemCount = 1, PartitionKey = lookup.NormalizedUserName };
            var query = GetQuery(queryOptions)
               .Where(u => u.NormalizedEmail == normalizedEmail && u.Type == typeof(TUser).Name);

            return await ReadFromQueryAsync(query, queryOptions, new ItemOptions() { });
        }

        private async Task<TUser> FindByIdInternalAsync(string userId)
        {
            var lookup = await GetNormalizedNameByIdAsync(userId);
            TUser foundUser = await ReadAsync(userId, lookup.NormalizedUserName);
            return foundUser;
        }

        private async Task<TUser> FindByLoginInternalAsync(string loginProvider, string providerKey)
        {
            var lookup = await GetNormalizedNameByLoginAsync(loginProvider, providerKey);
            if (lookup == null)
            {
                return null;
            }

            var queryOptions = new QueryOptions() { MaxItemCount = 1, PartitionKey = lookup.NormalizedUserName };
            var query = GetQuery(queryOptions)
                .SelectMany(u => u.Logins
                    .Where(l => l.LoginProvider == loginProvider && l.ProviderKey == providerKey)
                    .Select(l => u)
                );

            return await ReadFromQueryAsync(query, queryOptions, new ItemOptions() { });
        }

        private Task<DocumentUserEmaiLookup> GetNormalizedNameByEmailAsync(string normalizedEmail)
        {
            return _context.ReadAsync<DocumentUserEmaiLookup>(_collectionName, $"{normalizedEmail}_lookup",
                new ItemOptions() { PartitionKey = nameof(DocumentUserEmaiLookup) });
        }

        private Task<DocumentUserIdLookup> GetNormalizedNameByIdAsync(string userId)
        {
            return _context.ReadAsync<DocumentUserIdLookup>(_collectionName, $"{userId}_lookup",
                new ItemOptions() { PartitionKey = nameof(DocumentUserIdLookup) });
        }

        private Task<DocumentUserLoginLookup> GetNormalizedNameByLoginAsync(string loginProvider, string providerKey)
        {
            return _context.ReadAsync<DocumentUserLoginLookup>(_collectionName, $"{loginProvider}+{providerKey}_lookup",
                new ItemOptions() { PartitionKey = nameof(DocumentUserLoginLookup) });
        }

        private IAsyncEnumerable<TUser> GetUsersForClaimAsAsyncEnumerable(Claim claim, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (claim == null)
            {
                throw new ArgumentNullException(nameof(claim));
            }

            return GetUsersForClaimInternalAsAsyncEnumerable(claim);
        }

        private async IAsyncEnumerable<TUser> GetUsersForClaimInternalAsAsyncEnumerable(Claim claim)
        {
            var claimToLookup = new DocumentUserClaimsLookup.ClaimLookup() { Type = claim.Type, Value = claim.Value };
            var lookupsQuery = _context.GetQuery<DocumentUserClaimsLookup>(_collectionName, new QueryOptions()
            {
                PartitionKey = nameof(DocumentUserClaimsLookup)
            }).Where(x => x.NormalizedClaims.Contains(claimToLookup));

            var docType = typeof(DocumentDbIdentityUser).Name;
            await foreach (var lookup in _context.ToAsyncEnumerable(lookupsQuery))
            {
                var userQuery = GetQuery(new QueryOptions() { PartitionKey = lookup.NormalizedUserName, MaxItemCount = 1 })
                        .Where(r => r.Type == docType);

                yield return await _context.FirstOrDefaultAsync(userQuery);
            }
        }

        private IAsyncEnumerable<TUser> GetUsersInRoleAsAsyncEnumerable(string roleName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (roleName == null)
            {
                throw new ArgumentNullException(nameof(roleName));
            }

            return GetUsersInRoleInternalAsAsyncEnumerable(roleName);
        }

        private async IAsyncEnumerable<TUser> GetUsersInRoleInternalAsAsyncEnumerable(string roleName)
        {
            roleName = roleName.ToUpperInvariant();
            var lookupsQuery = _context.GetQuery<DocumentUserRolesLookup>(_collectionName, new QueryOptions()
            {
                PartitionKey = nameof(DocumentUserRolesLookup)
            }).Where(x => x.NormalizedRoleNames.Contains(roleName));

            var docType = typeof(DocumentDbIdentityUser).Name;
            await foreach (var lookup in _context.ToAsyncEnumerable(lookupsQuery))
            {
                var userQuery = GetQuery(new QueryOptions() { PartitionKey = lookup.NormalizedUserName, MaxItemCount = 1 })
                        .Where(r => r.Type == docType);

                yield return await _context.FirstOrDefaultAsync(userQuery);
            }
        }

        private async Task<bool> RedeemCodeInternalAsync(TUser user, string code, CancellationToken cancellationToken)
        {
            if (user.RecoveryCodes.Contains(code))
            {
                var codes = user.RecoveryCodes.Where(x => x != code);
                await ReplaceCodesAsync(user, codes, cancellationToken);
                return true;
            }
            return false;
        }

        private async Task RemoveClaimsInternalAsync(TUser user, IEnumerable<Claim> claims)
        {
            IEnumerable<Claim> foundClaims = user.Claims.Where(c => claims.Any(rc => rc.Type == c.Type && rc.Value == c.Value)).ToList();
            foreach (Claim claimToRemove in foundClaims)
            {
                user.Claims.Remove(claimToRemove);
            }

            await UpdatePartitionClaimsLookupAsync(user);
        }

        private async Task RemoveFromRoleInternalAsync(TUser user, string roleName)
        {
            roleName = roleName.ToUpperInvariant();
            TRole roleToRemove = user.Roles.FirstOrDefault(r => r.NormalizedName == roleName);

            if (roleToRemove != null)
            {
                user.Roles.Remove(roleToRemove);
                await UpdatePartitionRolesLookupAsync(user);
            }
        }

        private async Task ReplaceClaimInternalAsync(TUser user, Claim claim, Claim newClaim)
        {
            if (user.Claims.Any(c => c.Equals(claim)))
            {
                user.Claims.Remove(claim);
                user.Claims.Add(newClaim);
                await UpdatePartitionClaimsLookupAsync(user);
            }
        }

        private async Task<IdentityResult> UpdateInternalAsync(TUser user)
        {
            try
            {
                if (user.NormalizedUserName != user.PartitionKey)
                {
                    await UpdatePartitionLookupAsync(user);
                    var oldPk = user.PartitionKey;
                    user.PartitionKey = user.NormalizedUserName;
                    await CreateAsync(user);
                    await DeleteAsync(user.Id, oldPk);
                }
                else
                {
                    await ReplaceAsync(CreateOptions(user, new ItemOptions()
                    {
                        EnableContentResponseOnWrite = false,
                        IfMatchEtag = user.ETag
                    }), user);
                }
            }
            catch (CosmosException dce)
            {
                if (dce.StatusCode == HttpStatusCode.NotFound || dce.StatusCode == HttpStatusCode.PreconditionFailed)
                {
                    return IdentityResult.Failed(new IdentityError()
                    {
                        Description = nameof(HttpStatusCode),
                        Code = dce.StatusCode.ToString()
                    });
                }

                throw;
            }

            return IdentityResult.Success;
        }

        private async Task UpdatePartitionClaimsLookupAsync(TUser user)
        {
            await _context.UpsertAsync(_collectionName,
                new DocumentUserClaimsLookup()
                {
                    Id = $"{user.Id}_lookup",
                    PartitionKey = nameof(DocumentUserClaimsLookup),
                    NormalizedUserName = user.NormalizedUserName,
                    NormalizedClaims = user.Claims.Select(x => new DocumentUserClaimsLookup.ClaimLookup() { Type = x.Type, Value = x.Value }).ToArray()
                },
                new ItemOptions() { PartitionKey = nameof(DocumentUserClaimsLookup), EnableContentResponseOnWrite = false });
        }

        private async Task UpdatePartitionLookupAsync(TUser user)
        {
            await _context.UpsertAsync(_collectionName,
                new DocumentUserEmaiLookup()
                {
                    Id = $"{user.NormalizedEmail}_lookup",
                    PartitionKey = nameof(DocumentUserEmaiLookup),
                    NormalizedUserName = user.NormalizedUserName
                },
                new ItemOptions() { PartitionKey = nameof(DocumentUserEmaiLookup), EnableContentResponseOnWrite = false });

            await _context.UpsertAsync(_collectionName,
                new DocumentUserIdLookup()
                {
                    Id = $"{user.Id}_lookup",
                    PartitionKey = nameof(DocumentUserIdLookup),
                    NormalizedUserName = user.NormalizedUserName
                },
                new ItemOptions() { PartitionKey = nameof(DocumentUserIdLookup), EnableContentResponseOnWrite = false });

            await UpdatePartitionRolesLookupAsync(user);
            await UpdatePartitionClaimsLookupAsync(user);
            await UpdatePartitionLoginsLookupAsync(user);
        }

        private async Task UpdatePartitionLoginsLookupAsync(TUser user)
        {
            if (user.Logins.Count > 0)
            {
                var tLogins = _context.CreateTransaction(_collectionName, nameof(DocumentUserLoginLookup));
                foreach (var loginInfo in user.Logins)
                {
                    tLogins = tLogins.UpsertItem(new DocumentUserLoginLookup()
                    {
                        Id = $"{loginInfo.LoginProvider}+{loginInfo.ProviderKey}_lookup",
                        PartitionKey = nameof(DocumentUserLoginLookup),
                        NormalizedUserName = user.NormalizedUserName
                    }, new TransactionOptions() { EnableContentResponseOnWrite = false });
                }

                await foreach (var transactionResponse in _context.ExecuteTransactionAsync(tLogins))
                {
                    if (transactionResponse.Status != HttpStatusCode.OK && transactionResponse.Status != HttpStatusCode.Created)
                    {
                        throw new DocumentDbException("Failed to update lookups");
                    }
                }
            }
        }

        private async Task UpdatePartitionRolesLookupAsync(TUser user)
        {
            await _context.UpsertAsync(_collectionName,
                new DocumentUserRolesLookup()
                {
                    Id = $"{user.Id}_lookup",
                    PartitionKey = nameof(DocumentUserRolesLookup),
                    NormalizedUserName = user.NormalizedUserName,
                    NormalizedRoleNames = user.Roles.Select(x => x.NormalizedName).ToArray()
                },
                new ItemOptions() { PartitionKey = nameof(DocumentUserRolesLookup), EnableContentResponseOnWrite = false });
        }

        #region IDisposable Support

        /// <summary>
        ///
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Cleanup not required as the document context is injected.
        }

        #endregion IDisposable Support
    }
}