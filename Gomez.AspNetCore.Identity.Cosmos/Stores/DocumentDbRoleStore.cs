﻿using Gomez.AspNetCore.Identity.Cosmos.Models;
using Gomez.AspNetCore.Identity.Cosmos.Models.Lookups;
using Gomez.Core.DocumentDb;
using Microsoft.AspNetCore.Identity;
using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace Gomez.AspNetCore.Identity.Cosmos.Stores
{
    /// <summary>
    /// Represents a DocumentDb-based persistence store for ASP.NET Core Identity roles
    /// </summary>
    /// <typeparam name="TRole">The type representing a role</typeparam>
    public class DocumentDbRoleStore<TRole> : StoreBase<TRole>, IRoleClaimStore<TRole>
        where TRole : DocumentDbIdentityRole, IPartitionedDocumentEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentDbRoleStore{TRole}"/>
        /// </summary>
        /// <param name="dbContext">The DocumentDb client to be used</param>
        /// <param name="options">The configuraiton options for the <see cref="IDocumentClient"/></param>
        public DocumentDbRoleStore(IDocumentDbContext dbContext, IOptions<DocumentDbOptions> options)
            : base(dbContext, options.Value.RoleStoreDocumentCollection ?? options.Value.UserStoreDocumentCollection, x => x.PartitionKey)
        {
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="role"></param>
        /// <param name="claim"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task AddClaimAsync(TRole role, Claim claim, CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }

            if (claim == null)
            {
                throw new ArgumentNullException(nameof(claim));
            }

            role.Claims.Add(claim);

            return Task.CompletedTask;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="role"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<IdentityResult> CreateAsync(TRole role, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }

            // If no RoleId was specified, generate one
            if (role.Id == null)
            {
                role.Id = Guid.NewGuid().ToString();
            }

            return CreateInternalAsync(role);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public new Task<IdentityResult> DeleteAsync(TRole role)
        {
            return DeleteInternalAsync(role);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="role"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<IdentityResult> DeleteAsync(TRole role, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }

            return DeleteInternalAsync(role);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<TRole> FindByIdAsync(string roleId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (roleId == null)
            {
                throw new ArgumentNullException(nameof(roleId));
            }

            return FindByIdInternalAsync(roleId);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="normalizedRoleName"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<TRole> FindByNameAsync(string normalizedRoleName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (normalizedRoleName == null)
            {
                throw new ArgumentNullException(nameof(normalizedRoleName));
            }

            var query = GetQuery(new QueryOptions() { PartitionKey = normalizedRoleName })
                .Where(r => r.NormalizedName == normalizedRoleName && r.Type == typeof(TRole).Name);
            return _context.FirstOrDefaultAsync(query);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="role"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<IList<Claim>> GetClaimsAsync(TRole role, CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }

            return Task.FromResult(role.Claims);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="role"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<string> GetNormalizedRoleNameAsync(TRole role, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }

            return Task.FromResult(role.NormalizedName);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="role"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<string> GetRoleIdAsync(TRole role, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }

            return Task.FromResult(role.Id);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="role"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<string> GetRoleNameAsync(TRole role, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }

            return Task.FromResult(role.Name);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="role"></param>
        /// <param name="claim"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task RemoveClaimAsync(TRole role, Claim claim, CancellationToken cancellationToken = default)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }

            if (claim == null)
            {
                throw new ArgumentNullException(nameof(claim));
            }

            role.Claims.Remove(claim);

            return Task.CompletedTask;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="role"></param>
        /// <param name="normalizedName"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task SetNormalizedRoleNameAsync(TRole role, string normalizedName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }

            role.NormalizedName = normalizedName ?? throw new ArgumentNullException(nameof(normalizedName));

            return Task.CompletedTask;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="role"></param>
        /// <param name="roleName"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task SetRoleNameAsync(TRole role, string roleName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }

            role.Name = roleName ?? throw new ArgumentNullException(nameof(roleName));

            return Task.CompletedTask;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="role"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<IdentityResult> UpdateAsync(TRole role, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();

            if (role == null)
            {
                throw new ArgumentNullException(nameof(role));
            }

            return UpdateInternalAsync(role);
        }

        private async Task<IdentityResult> CreateInternalAsync(TRole role)
        {
            try
            {
                await UpdatePartitionLookupAsync(role);
                await CreateAsync(CreateOptions(role, new ItemOptions() { EnableContentResponseOnWrite = false }), role);
                return IdentityResult.Success;
            }
            catch (CosmosException ex)
            {
                return IdentityResult.Failed(new IdentityError() { Code = ex.ToString() });
            }
        }

        private async Task<IdentityResult> DeleteInternalAsync(TRole role)
        {
            try
            {
                await DeleteAsync(role);
            }
            catch (CosmosException dce)
            {
                if (dce.StatusCode == HttpStatusCode.NotFound)
                {
                    return IdentityResult.Failed();
                }

                throw;
            }

            return IdentityResult.Success;
        }

        private async Task<TRole> FindByIdInternalAsync(string roleId)
        {
            var lookup = await GetNormalizedNameByIdAsync(roleId);
            TRole role = await ReadAsync(roleId, lookup.NormalizedName);
            return role;
        }

        private Task<DocumentRoleIdLookup> GetNormalizedNameByIdAsync(string roleId)
        {
            return _context.ReadAsync<DocumentRoleIdLookup>(_collectionName, $"{roleId}_lookup",
                new ItemOptions() { PartitionKey = nameof(DocumentRoleIdLookup) });
        }

        private async Task<IdentityResult> UpdateInternalAsync(TRole role)
        {
            try
            {
                if (role.NormalizedName != role.PartitionKey)
                {
                    await UpdatePartitionLookupAsync(role);
                    var oldPk = role.PartitionKey;
                    role.PartitionKey = role.NormalizedName;
                    await CreateAsync(role);
                    await DeleteAsync(role.Id, oldPk);
                }
                else
                {
                    await ReplaceAsync(CreateOptions(role, new ItemOptions()
                    {
                        EnableContentResponseOnWrite = false,
                        IfMatchEtag = role.ETag
                    }), role);
                }
            }
            catch (CosmosException dce)
            {
                if (dce.StatusCode == HttpStatusCode.NotFound || dce.StatusCode == HttpStatusCode.PreconditionFailed)
                {
                    return IdentityResult.Failed(new IdentityError()
                    {
                        Description = nameof(HttpStatusCode),
                        Code = dce.StatusCode.ToString()
                    });
                }

                throw;
            }

            return IdentityResult.Success;
        }

        /// <summary>
        /// Used to lookup partitionKey when finding by id.
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        private Task UpdatePartitionLookupAsync(TRole role)
        {
            return _context.UpsertAsync(_collectionName,
                new DocumentRoleIdLookup()
                {
                    Id = $"{role.Id}_lookup",
                    PartitionKey = nameof(DocumentRoleIdLookup),
                    NormalizedName = role.NormalizedName
                },
                new ItemOptions() { PartitionKey = nameof(DocumentRoleIdLookup), EnableContentResponseOnWrite = false });
        }

        #region IDisposable Support

        /// <summary>
        ///
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            // Cleanup not required as the document context is injected.
        }

        #endregion IDisposable Support
    }
}