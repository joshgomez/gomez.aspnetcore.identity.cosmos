@echo off
cd %~dp0
nuget.exe pack Gomez.AspNetCore.Identity.Cosmos.csproj -IncludeReferencedProjects -Properties Configuration=Release -OutputDirectory "D:\NuGetPackages"