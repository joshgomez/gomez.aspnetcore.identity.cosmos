﻿namespace Gomez.AspNetCore.Identity.Cosmos
{
    /// <summary>
    /// The options this library
    /// </summary>
    public class DocumentDbOptions
    {
        /// <summary>
        /// Gets or sets the name of the DocumentCollection that should be used to store and query users in DocumentDb.
        /// </summary>
        public string UserStoreDocumentCollection { get; set; }

        /// <summary>
        /// Gets or sets the name of the DocumentCollection that should be used to store and query roles in DocumentDb.
        /// </summary>
        public string RoleStoreDocumentCollection { get; set; }
    }
}