﻿using Gomez.AspNetCore.Identity.Cosmos.Stores;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Gomez.AspNetCore.Identity.Cosmos.Extensions
{
    /// <summary>
    /// The extensions to enable this library
    /// </summary>
    public static class IdentityDocumentDbBuilderExtensions
    {
        /// <summary>
        /// Default configuration
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IdentityBuilder AddDocumentDbStores(this IdentityBuilder builder)
        {
            return builder.AddDocumentDbStores(setupAction: null);
        }

        /// <summary>
        /// Custom configuration
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="setupAction"></param>
        /// <returns></returns>
        public static IdentityBuilder AddDocumentDbStores(this IdentityBuilder builder, Action<DocumentDbOptions> setupAction)
        {
            if (setupAction != null)
            {
                builder.Services.Configure(setupAction);
            }

            builder.Services.AddSingleton(
                typeof(IRoleStore<>).MakeGenericType(builder.RoleType),
                typeof(DocumentDbRoleStore<>).MakeGenericType(builder.RoleType));

            builder.Services.AddSingleton(
                typeof(IUserStore<>).MakeGenericType(builder.UserType),
                typeof(DocumentDbUserStore<,>).MakeGenericType(builder.UserType, builder.RoleType));

            builder.Services.AddTransient<ILookupNormalizer, LookupNormalizer>();

            return builder;
        }
    }
}