﻿using Gomez.AspNetCore.Identity.Cosmos.Tools;
using Gomez.Core.DocumentDb;
using Gomez.Core.DocumentDb.Cosmos;
using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Gomez.AspNetCore.Identity.Cosmos.Extensions
{
    /// <summary>
    /// The database depedency required to use this library
    /// </summary>
    public static class IServiceCollectionExtensions
    {
        /// <summary>
        /// This will create a CosmosClient and IDocumentDbContext for dependency injection and is required when using this library.
        /// IDistributedCache and ILogger is required.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="connectionString">Must include the database</param>
        /// <param name="options"></param>
        /// <param name="serializerSettings"></param>
        /// <returns></returns>
        public static IServiceCollection AddDefaultDocumentClientForIdentity(this IServiceCollection services, string connectionString, CosmosClientOptions options = null, JsonSerializerSettings serializerSettings = null)
        {
            var config = new DbConnectionString(connectionString);
            services.AddSingleton<ICosmosOptions>(x => new DefaultCosmosOptions(x.GetService<IConfiguration>()));
            services.AddSingleton(x => CosmosClientFactory.CreateClient(connectionString, options, serializerSettings));
            services.AddSingleton<IDocumentDbContext>(x => new CosmosDbContext(
                x.GetService<CosmosClient>(),
                config.Database,
                x.GetService<IDistributedCache>(),
                x.GetService<ILogger<CosmosDbContext>>(),
                x.GetService<ICosmosOptions>()));

            return services;
        }
    }
}