﻿using Gomez.AspNetCore.Identity.Cosmos.Converters;
using Gomez.AspNetCore.Identity.Cosmos.Serializers;
using Microsoft.Azure.Cosmos;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Gomez.AspNetCore.Identity.Cosmos.Tools
{
    internal static class CosmosClientFactory
    {
        internal static CosmosClient CreateClient(string connectionString, CosmosClientOptions options, JsonSerializerSettings serializerSettings)
        {
            options ??= new CosmosClientOptions();
            serializerSettings ??= new JsonSerializerSettings();
            serializerSettings.Converters ??= new List<JsonConverter>();
            serializerSettings.Converters.Add(new JsonClaimConverter());
            serializerSettings.Converters.Add(new JsonClaimsPrincipalConverter());
            serializerSettings.Converters.Add(new JsonClaimsIdentityConverter());
            options.Serializer = new CosmosJsonDotNetSerializer(serializerSettings);

            return new CosmosClient(connectionString, options);
        }
    }
}