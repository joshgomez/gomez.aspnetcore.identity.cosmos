﻿using Microsoft.AspNetCore.Identity;

namespace Gomez.AspNetCore.Identity.Cosmos
{
    /// <summary>
    /// The lookup configuration for this library
    /// </summary>
    public class LookupNormalizer : ILookupNormalizer
    {
        /// <summary>
        /// NormalizeName
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string NormalizeName(string name)
        {
            return name.Normalize().ToUpperInvariant();
        }

        /// <summary>
        /// NormalizeEmail
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public string NormalizeEmail(string email)
        {
            return email.Normalize().ToUpperInvariant();
        }
    }
}